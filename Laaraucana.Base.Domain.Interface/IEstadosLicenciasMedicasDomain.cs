﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laaraucana.Base.Domain.Entity;

namespace Laaraucana.Base.Domain.Interface
{
    public interface IEstadosLicenciasMedicasDomain
    {
        Task<IEnumerable<EstadosLicenciasMedicas>> GetStatusAsync();
    }
}

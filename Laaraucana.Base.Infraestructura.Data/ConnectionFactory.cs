﻿using System;
using System.Data.Odbc;
using Laaraucana.Base.Transversal.Common;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;

namespace Laaraucana.Base.Infraestructura.Data
{
    public class ConnectionFactory: IConnectionFactory
    {
        private readonly IConfiguration _configuration;

        public ConnectionFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IDbConnection GetConnection {
            get
            {
                var sqlConnection = new OdbcConnection();
                if (sqlConnection == null) return null;

                sqlConnection.ConnectionString = _configuration.GetConnectionString("ConexionAS400");
                sqlConnection.Open();
                return sqlConnection;

            }
        }

    }
}
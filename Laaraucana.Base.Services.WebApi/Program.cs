using AutoMapper;
using Laaraucana.Base.Transversal.Mapper;
using Laaraucana.Base.Transversal.Common;
using Laaraucana.Base.Infraestructura.Data;
using Laaraucana.Base.Infraestructura.Repository;
using Laaraucana.Base.Infraestructura.Interface;
using Laaraucana.Base.Domain.Interface;
using Laaraucana.Base.Domain.Core;
using Laaraucana.Base.Application.Interface;
using Laaraucana.Base.Application.Main;
using Laaraucana.Base.Transversal.Logging;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
    
                        
builder.Services.AddAutoMapper(x => x.AddProfile(new MappingsProfile()));
builder.Services.Configure<Microsoft.AspNetCore.Http.Json.JsonOptions>(options =>
{
    options.SerializerOptions.PropertyNameCaseInsensitive = false;
    options.SerializerOptions.PropertyNamingPolicy = null;
    options.SerializerOptions.WriteIndented = true;
});

var configuration = builder.Configuration;

builder.Services.AddSingleton<IConfiguration>(configuration);
builder.Services.AddSingleton<IConnectionFactory, ConnectionFactory>();
builder.Services.AddScoped<IEstadosLicenciasMedicasApplication, EstadosLicenciasMedicasApplication>();
builder.Services.AddScoped<IEstadosLicenciasMedicasDomain,EstadosLicenciasMedicasDomain>();
builder.Services.AddScoped<IEstadosLicenciasMedicasReposirory, EstadosLicenciasMedicasRepository>();
builder.Services.AddScoped(typeof(IAppLogger<>), typeof(LoggerAdapter<>));


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Open API La Araucana",
        Description = "Una APi como plantilla que esta orientada al dominio (DDD)",        
    });
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseSwagger();
app.UseSwaggerUI();


app.UseAuthorization();

app.MapControllers();

app.Run();

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;
using Laaraucana.Base.Application.DTO;
using Laaraucana.Base.Application.Interface;
namespace Laaraucana.Base.Services.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EstadosLicenciasMedicasController : Controller
    {
        private readonly IEstadosLicenciasMedicasApplication _estadosLicenciasMedicasApplication;

        public EstadosLicenciasMedicasController(IEstadosLicenciasMedicasApplication estadosLicenciasMedicasApplication)
        {
            _estadosLicenciasMedicasApplication = estadosLicenciasMedicasApplication;            
        }

        /// <summary>
        /// Obtiene el estado de la licencia medicas por afiliado.
        /// </summary>        
        /// <returns></returns>
        /// 
        [HttpGet]        
        public async Task<IActionResult> GetStatusAsync()
        {
            var response = await _estadosLicenciasMedicasApplication.GetStatusAsync();
            if (response.IsSuccess)
                return Ok(response);

            return BadRequest(response.Message);            
        }

        [HttpGet]
        public async Task<IActionResult> GetMensaggeAsync()
        {
            return Ok("Primera prueba");
            /*var response = await _estadosLicenciasMedicasApplication.GetStatusAsync();
            if (response.IsSuccess)
                return Ok(response);

            return BadRequest(response.Message);*/
        }

    }
}

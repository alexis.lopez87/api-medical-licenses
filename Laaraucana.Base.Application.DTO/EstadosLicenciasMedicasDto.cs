﻿namespace Laaraucana.Base.Application.DTO
{
    public class EstadosLicenciasMedicasDto
    {
        public string AFINOM { get; set; }
        public int? OFICOD { get; set; }
        public int LICDIAS { get; set; }

    }
}
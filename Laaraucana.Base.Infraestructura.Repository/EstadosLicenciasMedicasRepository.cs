﻿using System;
using Laaraucana.Base.Domain.Entity;
using Laaraucana.Base.Infraestructura.Interface;
using Laaraucana.Base.Transversal.Common;
using Dapper;
using System.Data;
using System.Threading.Tasks;

namespace Laaraucana.Base.Infraestructura.Repository
{
    public class EstadosLicenciasMedicasRepository: IEstadosLicenciasMedicasReposirory
    {
        private readonly IConnectionFactory _connectionFactory;

        public EstadosLicenciasMedicasRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public async Task<IEnumerable<EstadosLicenciasMedicas>> GetStatusAsync()
        {
            using(var connection = _connectionFactory.GetConnection)
            {
                var sql = @"SELECT * FROM LIEXP.ILF1250 LIMIT 10";
                var result = await connection.QueryAsync<EstadosLicenciasMedicas>(sql);
                return result;
            }
        }

    }
}
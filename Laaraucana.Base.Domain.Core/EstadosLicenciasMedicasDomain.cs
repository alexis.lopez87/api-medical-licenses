﻿using System;
using Laaraucana.Base.Domain.Entity;
using Laaraucana.Base.Domain.Interface;
using Laaraucana.Base.Infraestructura.Interface;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Laaraucana.Base.Domain.Core
{
    public class EstadosLicenciasMedicasDomain: IEstadosLicenciasMedicasDomain
    {
        private readonly IEstadosLicenciasMedicasReposirory _estadosLicenciasMedicasReposirory;

        public EstadosLicenciasMedicasDomain(IEstadosLicenciasMedicasReposirory estadosLicenciasMedicasReposirory)
        {
            _estadosLicenciasMedicasReposirory = estadosLicenciasMedicasReposirory;

        }

        public async Task<IEnumerable<EstadosLicenciasMedicas>> GetStatusAsync()
        {
            return await _estadosLicenciasMedicasReposirory.GetStatusAsync();
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laaraucana.Base.Application.DTO;
using Laaraucana.Base.Transversal.Common;
namespace Laaraucana.Base.Application.Interface
{
    public interface IEstadosLicenciasMedicasApplication
    {
        Task<Response<IEnumerable<EstadosLicenciasMedicasDto>>> GetStatusAsync();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laaraucana.Base.Domain.Entity;
namespace Laaraucana.Base.Infraestructura.Interface
{
    public interface IEstadosLicenciasMedicasReposirory
    {
        #region Métodos Asincronos
        Task<IEnumerable<EstadosLicenciasMedicas>> GetStatusAsync();

        #endregion

    }
}

﻿using System;
using AutoMapper;
using Laaraucana.Base.Application.DTO;
using Laaraucana.Base.Application.Interface;
using Laaraucana.Base.Domain.Entity;
using Laaraucana.Base.Domain.Interface;
using Laaraucana.Base.Transversal.Common;
using System.Threading.Tasks;
using System.Collections.Generic;
namespace Laaraucana.Base.Application.Main
{
    public class EstadosLicenciasMedicasApplication: IEstadosLicenciasMedicasApplication
    {
        private readonly IEstadosLicenciasMedicasDomain _estadosLicenciasMedicasDomain;
        private readonly IMapper _mapper;
        private readonly IAppLogger<EstadosLicenciasMedicasApplication> _logger;

        public EstadosLicenciasMedicasApplication(IMapper mapper, IEstadosLicenciasMedicasDomain estadosLicenciasMedicasDomain, IAppLogger<EstadosLicenciasMedicasApplication> logger)
        {
            _mapper = mapper;
            _estadosLicenciasMedicasDomain = estadosLicenciasMedicasDomain;
            _logger = logger;
        }


        public async Task<Response<IEnumerable<EstadosLicenciasMedicasDto>>> GetStatusAsync()
        {
            var response = new Response<IEnumerable<EstadosLicenciasMedicasDto>>();

            try
            {
                var EstadosLicenciasMedicas = await _estadosLicenciasMedicasDomain.GetStatusAsync();
                response.Data = _mapper.Map<IEnumerable<EstadosLicenciasMedicasDto>>(EstadosLicenciasMedicas);
                if(response.Data != null)
                {
                    response.IsSuccess = true;
                    response.Message = "Consulta Exitosa";                    
                }
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return response;
        }
    }
}